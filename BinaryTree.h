#pragma once
#include<string>
#include<string.h>
#include<vector>
#include<stack>
#include <set>

using namespace std;

class Node {
public:
	string city = "";
	Node *left;  
	Node *right; 
	bool left_visited  = false;
	bool right_visited = false;
	int left_cost;
	int right_cost;
	int cost = 2000;
	Node(string city) 
	{
		this->city = city;
	}
	static Node* getTheNode(string desired_node, vector<Node*> &all_nodes);
};

class BinaryTree {
public:
	Node * root = NULL;
	void merge(string a , string b,string c,vector<Node*> &all_nodes);                      // To merge two nodes and place the new node conveniently 
	void calculateCost(Node* incoming);
	int cost;
	stack<Node*> stack_nodes;
	vector<Node*>vec;
	string traversal_path = "";
	vector<Node*> all_nodes;
};

class Result {
public:
	string city_name;
	int cost = 2000;
};
