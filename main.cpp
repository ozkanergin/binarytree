// HW2_City_Problem.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <string>
#include <stack>
#include <sstream>
#include "BinaryTree.h"

int main(int argc, char *argv[])
{
	Result result;
#pragma region FIRST PERSON
	BinaryTree binary_tree;
	string first_file = "";
	string second_file = "";

	if (argv[2] != NULL) 
	{
		 first_file = argv[1];
		 second_file = argv[2];
	}

	ifstream file_obj; //object to read file
	file_obj.open(first_file, ifstream::in);
	if (!file_obj.is_open()) {
		file_obj.open("map1_friend1.txt", ifstream::in);
		if (!file_obj.is_open()) {
			cerr << "Could not open the file" << endl;
			exit(1);
		}
	}
	string line;
	while (getline(file_obj, line))
	{
		std::istringstream ss(line);
		string substr = "";
		string first_parameter = "";
		string second_parameter = "";
		string third_parameter = "";
		int indeks = 1;
		while (getline(ss, substr, ' '))
		{
			if (indeks == 1) {
				first_parameter = substr;
			}
			else if (indeks == 2) {
				second_parameter = substr;
			}
			else if (indeks == 3)
			{
				third_parameter = substr;
			}
			indeks++;
		}
		if (first_parameter != "")
		{
			binary_tree.merge(first_parameter, second_parameter, third_parameter, binary_tree.all_nodes);
		}
	}
	binary_tree.calculateCost(binary_tree.root);

	cout << "FRIEND-1:";
	for (Node* it : binary_tree.vec)
	{
		cout << it->city << ' ';
	}
	cout << '\n';
#pragma endregion

#pragma region SECOND PERSON
	BinaryTree binary_tree_2;

	ifstream file_obj_2;
	file_obj_2.open(second_file, ifstream::in);

	if (!file_obj_2.is_open()) {
		file_obj_2.open("map1_friend2.txt", ifstream::in);
		if (!file_obj_2.is_open()) {
			cerr << "Could not open the file" << endl;
			exit(1);
		}
	}

	line = "";
	while (getline(file_obj_2, line))
	{
		std::istringstream ss(line);
		string substr = "";
		string first_parameter = "";
		string second_parameter = "";
		string third_parameter = "";
		int indeks = 1;
		while (getline(ss, substr, ' '))
		{
			if (indeks == 1) {
				first_parameter = substr;
			}
			else if (indeks == 2) {
				second_parameter = substr;
			}
			else if (indeks == 3)
			{
				third_parameter = substr;
			}
			indeks++;
		}
		if (first_parameter != "")
		{
			binary_tree_2.merge(first_parameter, second_parameter, third_parameter, binary_tree_2.all_nodes);
		}
	}
	binary_tree_2.calculateCost(binary_tree_2.root);

	cout << "FRIEND-2:";
	for (Node* it : binary_tree_2.vec) {

		cout << it->city << ' ';
	}
	cout << '\n';

#pragma endregion

	for (int i = 0; i < (int)binary_tree.all_nodes.size(); i++)
	{
		for (int j = 0; j < (int)binary_tree_2.all_nodes.size(); j++)
		{
			if (binary_tree.all_nodes[i]->city == binary_tree_2.all_nodes[j]->city) {

				if (binary_tree.all_nodes[i]->cost + binary_tree_2.all_nodes[j]->cost < result.cost)
				{
					result.city_name = binary_tree.all_nodes[i]->city;
					result.cost = binary_tree.all_nodes[i]->cost + binary_tree_2.all_nodes[j]->cost;
				}
			}
		}
	}
	cout << "MEETING POINT:" << result.city_name << "\n";
	cout << "TOTAL DURATION COST:" << result.cost;
}

