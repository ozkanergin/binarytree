
#include "BinaryTree.h"
#include <algorithm>


void BinaryTree::merge(string first_node, string secon_node, string cost, vector<Node*> &all_nodes)
{
	Node* first_new_node = Node::getTheNode(first_node, all_nodes);
	Node* second_new_node = Node::getTheNode(secon_node, all_nodes);

	if (first_new_node == NULL)
	{
		first_new_node = new Node(first_node);
		all_nodes.push_back(first_new_node);
	}

	if (second_new_node == NULL) {
		second_new_node = new Node(secon_node);
		all_nodes.push_back(second_new_node);
	}

	if (root == NULL) {
		first_new_node->cost = 0;
		root = first_new_node;
	}

	if (first_new_node->right == NULL) 
	{
		first_new_node->right      = second_new_node;
		first_new_node->right_cost = std::stoi(cost);
	}
	else 
	{
		Node* temp = first_new_node->right;
		int cost_Temp = first_new_node->right_cost;

		first_new_node->left	= temp;
		first_new_node->left_cost = cost_Temp;

		first_new_node->right	   = second_new_node;
		first_new_node->right_cost = std::stoi(cost);
	}
}

Node* Node::getTheNode(string city, vector<Node*> &all_nodes)
{
	for (int i = 0; i < all_nodes.size(); i++)
	{
		if (all_nodes[i]->city == city)
		{
			return all_nodes[i];
		}
	}
	
	return NULL;
}

void BinaryTree::calculateCost(Node* incoming_node) 
{
	if (std::find(vec.begin(), vec.end(), incoming_node) == vec.end())
	{
		vec.push_back(incoming_node);
	}

	if (incoming_node->left != NULL && incoming_node->left_visited == false) 
	{
		incoming_node->left_visited = true;
		cost += incoming_node->left_cost;

		if (incoming_node->left->cost > cost) {
			incoming_node->left->cost = cost;
		}		
		
		traversal_path += "L";
		stack_nodes.push(incoming_node);

		calculateCost(incoming_node->left);
	}
	else if (incoming_node->right != NULL && incoming_node->right_visited == false)
	{
		incoming_node->right_visited = true;
		cost += incoming_node->right_cost;

		if (incoming_node->right->cost > cost) {
			incoming_node->right->cost = cost;
		}
		traversal_path += "R";
		stack_nodes.push(incoming_node);
		calculateCost(incoming_node->right);
	}
	else 
	{
		if (stack_nodes.empty() == true) return;
		Node* former_node = stack_nodes.top();
		if (traversal_path.back() == 'R') {
			cost -= former_node->right_cost;
		}
		else {
			cost -= former_node->left_cost;
		}
		stack_nodes.pop();
		traversal_path.erase(traversal_path.size() - 1);
		calculateCost(former_node);
	}
}